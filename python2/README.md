# Django GitLab

Application to integrate Django with GitLab


As GitLab becomes your single-source for DevOps Tooling, it becomes beneficial to
integrate GitLab into your application to provide visibility into how
operational activities are doing.

This application...

* Gives you the ability to create a Sentry Error in the app
* Shows which Unleash feature flags are active (according to the app)
* Shows you the version of GitLab the application was deployed from, the version of the  Deployment Runner, and the Commit SHA of the App
* Gives you the ability to save local environment variables into your GitLab CI Variables
* Gives you direct links to Feature Flags and Error Tracking Pages in GitLab.

## How This Is Built

Using the `.gitlab-ci.yml` modified the Auto DevOps YAML to pass GitLab CI Variables into the Deployed Application

